﻿using UnityEngine;
using System.Collections;

public class bluebird : MonoBehaviour {
	public GameObject longbird;
	public GameObject redbirdfeather;
	public Sprite SpriteShowWhenHurt;
	public GameObject birdsmoke;

	void OnCollisionEnter2D(Collision2D col)
	{

		if (col.gameObject.tag == "Brick" || col.gameObject.tag == "Ground") {
			StartCoroutine(DestroyAfter(1f));
			Instantiate(birdsmoke, transform.position, Quaternion.identity);
			GetComponent<Animator>().enabled = false;
			GetComponent<SpriteRenderer>().sprite = SpriteShowWhenHurt;
			
			GameObject a = Instantiate(longbird, new Vector3( transform.position.x, transform.position.y, 0), Quaternion.identity) as GameObject;
			Instantiate(redbirdfeather, new Vector3( transform.position.x, transform.position.y, 0), Quaternion.identity) ;
			
			a.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, Random.Range(10,20)));
			Instantiate(birdsmoke, transform.position, Quaternion.identity);
			
		}
	}
	IEnumerator DestroyAfter(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		Destroy(gameObject);
	}
}
