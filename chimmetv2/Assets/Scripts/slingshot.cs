﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using System;

public class slingshot : MonoBehaviour {
   
//	[HideInInspector]
    public GameObject BirdToThrown;
	public Vector3 SlingshotMiddleVector;
    [HideInInspector]
    public SlingshotState slingshotstate;
    public Transform LeftTransform, RightTransform;
	public LineRenderer TrajectoryLineRenderer;
    public LineRenderer SlingshotLineRenderer1;
    public LineRenderer SlingshotLineRenderer2;
	public GameObject bluebird;

    public Transform BirdWaitPos;
	public float ThrowSpeed = 7;

    [HideInInspector]
    public float TimeSinceThrown;
	private Vector3 temp;
	private float radius;
	private GameObject bluebird1;
	private GameObject bluebird2;
	public GameObject egg;
	public Sprite hurt;
	public Sprite speedup;
	public GameObject blacksmoke;
	private GameObject blackbird;

	public float bankinh = 50.0f;
	public float power = 100.0f;
	
    void Start()
    {
		TrajectoryLineRenderer.sortingLayerName = "foreground";
        SlingshotLineRenderer1.sortingLayerName = "foreground";
        SlingshotLineRenderer2.sortingLayerName = "foreground";

        slingshotstate = SlingshotState.Idle;
        SlingshotLineRenderer1.SetPosition(0, LeftTransform.position);
        SlingshotLineRenderer2.SetPosition(0, RightTransform.position);
        SlingshotLineRenderer1.SetPosition(1, BirdWaitPos.transform.position);
        SlingshotLineRenderer2.SetPosition(1, BirdWaitPos.transform.position);

        SlingshotLineRenderer1.sortingOrder = 3;
        SlingshotLineRenderer2.sortingOrder = 1;

        SlingshotMiddleVector = new Vector3((LeftTransform.position.x + RightTransform.position.x) / 2,
            (LeftTransform.position.y + RightTransform.position.y) / 2, 0);
    }

    void Update()
    {
        switch (slingshotstate)
        {
            case SlingshotState.Idle:
                InitBird();                
                DisplaySlingshotLineRenderer();
                if (Input.GetMouseButtonDown(0))
                {
                    Vector3 location = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    if (BirdToThrown.GetComponent<CircleCollider2D>() == Physics2D.OverlapPoint(location))
                    {
                        slingshotstate = SlingshotState.UserPullling;
						GetComponent<AudioSource>().Play ();
                    }
                }
                break;
            case SlingshotState.UserPullling:
                DisplaySlingshotLineRenderer();
                if (Input.GetMouseButton(0))
                {
                    Vector3 location = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    location.z = 0;
                    if (Vector3.Distance(location, SlingshotMiddleVector) > 1.5f)
                    {
                        var maxPosition = (location - SlingshotMiddleVector).normalized * 1.5f + SlingshotMiddleVector;
                        BirdToThrown.transform.position = maxPosition;
                    }
                    else
                    {
                        BirdToThrown.transform.position = location;
                    }
                    float distance = Vector3.Distance(SlingshotMiddleVector, BirdToThrown.transform.position);
				DisplayTrajectoryLineRenderer2(distance);
                }
                else
                {
				SetTrajectoryLineRenderesActive(false);
                    TimeSinceThrown = Time.time;
                    float distance = Vector3.Distance(SlingshotMiddleVector, BirdToThrown.transform.position);
                    if (distance > 1)
                    {
                        SetSlingshotLineRendererActive(false);
                        slingshotstate = SlingshotState.BirdFlying;
						ThrowBird(distance);
                    }
                    else
                    {
                        BirdToThrown.transform.positionTo(distance / 10, //duration
                            BirdWaitPos.transform.position). //final position
                            setOnCompleteHandler((x) =>
                            {
                                x.complete();
                                x.destroy();
                                InitBird();
                            });
                    }
                }
                break;
            case SlingshotState.BirdFlying:
			if(BirdToThrown.GetComponent<redbird>().BirdStyle == 1)     //yellowbird
			{
				if(Input.GetMouseButtonDown(0))
				{
					ThrowSpeed = 1.5f;  
					ThrowYellowBird(ThrowSpeed); 
					BirdToThrown.GetComponent<SpriteRenderer>().sprite = speedup;
					ThrowSpeed = 7;
				}
			}
			else if(BirdToThrown.GetComponent<redbird>().BirdStyle == 2)     //bluebird
			{
				if(Input.GetMouseButtonDown(0))
				{
					GameObject bluebird1 = Instantiate(bluebird, BirdToThrown.transform.position, Quaternion.identity) as GameObject;
					GameObject bluebird2 = Instantiate(bluebird, BirdToThrown.transform.position, Quaternion.identity) as GameObject;
					bluebird1.GetComponent<Rigidbody2D>().AddForce(new Vector2(bluebird.transform.position.x+ 350 , bluebird.transform.position.y + 350));
					bluebird2.GetComponent<Rigidbody2D>().AddForce(new Vector2(bluebird.transform.position.x + 350 , bluebird1.transform.position.y - 350));
					ThrowSpeed = 1.5f;  
					ThrowYellowBird(ThrowSpeed); 
					ThrowSpeed = 7;
				}
			}
			else if(BirdToThrown.GetComponent<redbird>().BirdStyle == 3)      //whitebird
			{
				if(Input.GetMouseButtonDown(0))
				{
					Instantiate(egg, BirdToThrown.transform.position, Quaternion.identity);
					ThrowSpeed = 1f;
					ThrowWhiteBird(ThrowSpeed);
					ThrowSpeed = 7f;
				}
			}
			else if(BirdToThrown.GetComponent<redbird>().BirdStyle == 4)
			{
				blackbird = BirdToThrown;
				StartCoroutine(DestroyAfter(3f));
				blackbird.GetComponent<Rigidbody>().AddExplosionForce(power, blackbird.transform.position, bankinh, 3f);
			} 
              break;
            default:
                break;
        }
    }
	IEnumerator DestroyAfter(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		Destroy(blackbird);
		Instantiate(blacksmoke, blackbird.transform.position, Quaternion.identity);
	}
	IEnumerator DestroyAfter1(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		Destroy(bluebird1);
	}
	IEnumerator DestroyAfter2(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		Destroy(bluebird2);
	}
	public void ThrowBird(float distance)
	{
		Vector3 velocity = SlingshotMiddleVector - BirdToThrown.transform.position;    
		BirdToThrown.GetComponent<redbird> ().Onthrow ();
		BirdToThrown.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity.x, velocity.y) * ThrowSpeed *distance;
		if (BirdThrown != null)
			BirdThrown(this, EventArgs.Empty);
	}
	
	public void ThrowYellowBird(float ThrowSpeed)
	{

		Vector3 velocity =  BirdToThrown.GetComponent<Rigidbody2D>().velocity;   
		BirdToThrown.GetComponent<redbird> ().Onthrow ();	
		BirdToThrown.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity.x, velocity.y) * ThrowSpeed;

		if (BirdThrown != null)
			BirdThrown(this, EventArgs.Empty);
	}
	public void ThrowWhiteBird(float ThrowSpeed)
	{
		Vector3 velocity =  BirdToThrown.GetComponent<Rigidbody2D>().velocity;   
		BirdToThrown.GetComponent<redbird> ().Onthrow ();	
		BirdToThrown.GetComponent<Rigidbody2D> ().velocity = new Vector2 (velocity.x, velocity.y) * ThrowSpeed ;
		BirdToThrown.GetComponent<Rigidbody2D> ().AddForce (new Vector2(BirdToThrown.transform.position.x, BirdToThrown.transform.position.y + 300));
		if (BirdThrown != null)
			BirdThrown(this, EventArgs.Empty);
	}

    public event EventHandler BirdThrown;
    private void InitBird()
    {
        BirdToThrown.transform.position = BirdWaitPos.position;
        slingshotstate = SlingshotState.Idle;
        SetSlingshotLineRendererActive(true);
    }
    void DisplaySlingshotLineRenderer()
    {
		temp.x = BirdToThrown.transform.position.x - 0.15f;
		temp.y = BirdToThrown.transform.position.y -0.15f;
		temp.z = 0;
		SlingshotLineRenderer1.SetPosition(1,temp);
		SlingshotLineRenderer2.SetPosition(1, temp);
    }
    void SetSlingshotLineRendererActive(bool active)
    {
        SlingshotLineRenderer1.enabled = active;
        SlingshotLineRenderer2.enabled = active;
    }
	public void SetTrajectoryLineRenderesActive(bool active)
	{
		TrajectoryLineRenderer.enabled = active;
	}
	
	void DisplayTrajectoryLineRenderer2(float distance)
	{
		SetTrajectoryLineRenderesActive(true);
		Vector3 v2 = SlingshotMiddleVector - BirdToThrown.transform.position;
		int segmentCount = 15;
		float segmentScale = 2;
		Vector2[] segments = new Vector2[segmentCount];

		segments[0] = BirdToThrown.transform.position;
	
		Vector2 segVelocity = new Vector2(v2.x, v2.y) * ThrowSpeed * distance;
		
		float angle = Vector2.Angle(segVelocity, new Vector2(1, 0));
		float time = segmentScale / segVelocity.magnitude;
		for (int i = 1; i < segmentCount; i++)
		{
			float time2 = i * Time.fixedDeltaTime * 5;
			segments[i] = segments[0] + segVelocity * time2 + 0.5f * Physics2D.gravity * Mathf.Pow(time2, 2);
		}
		TrajectoryLineRenderer.SetVertexCount(segmentCount);
		for (int i = 0; i < segmentCount; i++)
			TrajectoryLineRenderer.SetPosition(i, segments[i]);
	}

}  