﻿using UnityEngine;
using System.Collections;

public class smokecontroller : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (DestroyAfter (0.4f));
	}

	IEnumerator DestroyAfter(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		Destroy(gameObject);
	}
}
