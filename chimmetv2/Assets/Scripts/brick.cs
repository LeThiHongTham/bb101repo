﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class brick : MonoBehaviour {
	float Health = 150f;
	public Text m_text;
	public Text secondtext;
	gamanager gm;
	void Start()
	{
		GameObject game = GameObject.FindGameObjectWithTag ("manager");
		gm = game.GetComponent<gamanager> ();
	}
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.GetComponent<Rigidbody2D> () == null)
			return;
		float damage = coll.gameObject.GetComponent<Rigidbody2D> ().velocity.magnitude * 10;
		if (damage >= 10) {
			GetComponent<AudioSource> ().Play ();
		}
		Health -= damage;
		if (Health <= 3) {
			Destroy (this.gameObject);
			gm.score += 150;
			m_text.text = "SCORE: " + gm.score.ToString ();
			secondtext.text = gm.score.ToString();
		}
	}
}
