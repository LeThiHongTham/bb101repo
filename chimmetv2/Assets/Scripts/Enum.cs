﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Linq;
namespace Assets.Scripts
{
    public enum SlingshotState
    {
        Idle,
        UserPullling,
        BirdFlying,
    }
    public enum GameState
    {
        Start,
        BirdMovingToSlingshot,
        Playing,
        Won,
        Lost
    }
    public enum BirdState
    {
        BeforeThrown,
        Thrown
    }
}