﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
using UnityEngine.UI;
public class redbird : MonoBehaviour {
	public Sprite SpriteShowWhenHurt;
	public Sprite SpriteFlying;
	public slingshot slingshot;
	public GameObject longbird;
	public GameObject redbirdfeather;
	public Text m_text;
	public GameObject birdsmoke;
	public int BirdStyle;
	void Start () {

		GetComponent<TrailRenderer>().enabled = false;
		GetComponent<TrailRenderer>().sortingLayerName = "foreground";
		GetComponent<Rigidbody2D>().isKinematic = true;
		GetComponent<CircleCollider2D>().radius = 0.5f;
		State = BirdState.BeforeThrown;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//Neu da ban bird va toc do rat nho thi destroy bird.
		if (State == BirdState.Thrown && GetComponent<Rigidbody2D>().velocity.sqrMagnitude <= 0.05f)
		{
			StartCoroutine(DestroyAfter(1));
			Instantiate(birdsmoke, transform.position, Quaternion.identity);
		}
	}
	//Ham dc goi ben slingshot
	public void Onthrow()
	{
		GetComponent<AudioSource>().Play();
		GetComponent<TrailRenderer>().enabled = true;
		GetComponent<Rigidbody2D>().isKinematic = false;
		GetComponent<CircleCollider2D>().radius = 0.35f;
		State = BirdState.Thrown;
		GetComponent<Animator> ().enabled = false;
		GetComponent<SpriteRenderer> ().sprite = SpriteFlying;
	}
	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Brick" || col.gameObject.tag == "Ground") {
			GetComponent<Animator>().enabled = false;
			GetComponent<SpriteRenderer>().sprite = SpriteShowWhenHurt;
			slingshot.TrajectoryLineRenderer.enabled = false;

			GameObject a = Instantiate(longbird, new Vector3( transform.position.x, transform.position.y, 0), Quaternion.identity) as GameObject;
			Instantiate(redbirdfeather, new Vector3( transform.position.x, transform.position.y, 0), Quaternion.identity) ;

			a.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, Random.Range(10,20)));
			Instantiate(birdsmoke, transform.position, Quaternion.identity);

		}
	}
	IEnumerator DestroyAfter(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		Destroy(gameObject);
	}
	public BirdState State
	{
		get;
		private set;
	}
}
