﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;
public class follow : MonoBehaviour {

    public bool IsFollow;
    //[HideInInspector]
    public Transform BirdToFollow;
    public Transform farLeft;
    public Transform farRight;
   
    [HideInInspector]
    public Vector3 StartPos;
	//public GameObject smoke;
    void Start()
    {
        StartPos = transform.position;
    }
    void Update()
    {
        if (IsFollow)
        {
            if (BirdToFollow != null)
            {
                var birdPos = BirdToFollow.transform.position;
                float x = Mathf.Clamp(birdPos.x, farLeft.position.x, farRight.position.x);
                transform.position = new Vector3(x, StartPos.y, StartPos.z);
			//	Instantiate(smoke,BirdToFollow.transform.position, Quaternion.identity);
            }
            else
            {
                IsFollow = false;
            }
        }
    }
}
