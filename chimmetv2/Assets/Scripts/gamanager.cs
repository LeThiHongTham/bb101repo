﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using UnityEngine.UI;
public class gamanager : MonoBehaviour {

	public GameObject pausebut;
	public GameObject Return;
    public follow cameraFollow;
    public slingshot slingshot;
    public int currentIndex = 0;

    [HideInInspector]
    public  GameState CurrentGameState;
    private List<GameObject> Birds;
    private List<GameObject> Bricks;
    private List<GameObject> Pigs;

    public Vector3 StartPos;
	public GameObject mark;
	private Vector3 delta;
	public GameObject scoreboard;
	public GameObject lostboard;
	public float score = 0;
	public Text m_text;
	public Text secondtext;
	public GameObject leftstar;
	public GameObject rightstar;
	public GameObject midstar;
	public GameObject leftrong;
	public GameObject rightrong;
	public GameObject midrong;
    void Start()
    {
		StartPos.x =  -0.48f;
        StartPos.y = 0;
        StartPos.z = -10;
        CurrentGameState = GameState.Start;

        slingshot.enabled = true;
        Birds = new List<GameObject>(GameObject.FindGameObjectsWithTag("Bird"));
        Bricks = new List<GameObject>(GameObject.FindGameObjectsWithTag("Brick"));
        Pigs = new List<GameObject>(GameObject.FindGameObjectsWithTag("Pig"));

        slingshot.BirdThrown -= Slingshot_BirdThrown; slingshot.BirdThrown += Slingshot_BirdThrown;

		Camera.main.transform.position = new Vector3(9f,0f,-10f);
		Camera.main.transform.positionTo
			(3f, StartPos);
		scoreboard.SetActive (false);
		lostboard.SetActive (false);
    }
    void Update()
    {
        switch (CurrentGameState)
        {
            case GameState.Start:
                     MoveToSlingshot();
					 m_text.text = "SCORE: " + score.ToString();
                break;
            case GameState.BirdMovingToSlingshot:
                break;
            case GameState.Playing:
                if (slingshot.slingshotstate == SlingshotState.BirdFlying
                    && (BrickBirdPigStoppedMoving() || Time.time - slingshot.TimeSinceThrown > 5f))
                {
                    slingshot.enabled = false;
                    MoveToStartPos();
                    CurrentGameState = GameState.BirdMovingToSlingshot;
                }
                break;		
			case GameState.Won:
			{
				pausebut.SetActive (false);
				Return.SetActive (false);
				StartCoroutine(showscoreboard(1f));
			}
				break;
			case GameState.Lost:
			{
				pausebut.SetActive (false);
				Return.SetActive (false);
				lostboard.SetActive(true);
			}
			break;
            default:
                break;
        }
    }
    private void MoveToStartPos()
    {
        float duration = Vector2.Distance(Camera.main.transform.position, StartPos) / 10f;
        if (duration == 0.0f) duration = 0.1f;
        //animate the camera to start
        Camera.main.transform.positionTo
            (duration, StartPos). //end position
            setOnCompleteHandler((x) =>
            {
                cameraFollow.IsFollow = false ;
                if ( AllPigDestroyed())
                {
						GetComponent<AudioSource>().Play ();
						if(currentIndex == 1)		//con mot con chim
						{
							delta.x = Birds[currentIndex + 1].transform.position.x + 0;
							delta.y = Birds[currentIndex + 1].transform.position.y + 2;
							Instantiate(mark, delta, Quaternion.identity);

							StartCoroutine(showleftstar(1f));
							StartCoroutine(showmidstar(2f));
						}
						else if(currentIndex == 0)   //con hai con chim
						{
							delta.x = Birds[currentIndex + 1].transform.position.x + 0;
							delta.y = Birds[currentIndex + 1].transform.position.y + 2;
							Instantiate(mark, delta, Quaternion.identity);
							delta.x = Birds[currentIndex + 1].transform.position.x + 3;
							delta.y = Birds[currentIndex + 1].transform.position.y + 2;
							Instantiate(mark, delta, Quaternion.identity);

							StartCoroutine(showleftstar(1f));
							StartCoroutine(showmidstar(2f));
							StartCoroutine(showrightstar(3f));

						}
						else if(currentIndex == 2)
						{
							scoreboard.SetActive(false);

							leftrong.SetActive(true);
							rightrong.SetActive(true);
							midrong.SetActive(true);

							StartCoroutine(showleftstar(1f));		
						}
						CurrentGameState = GameState.Won;
                }
                else if (currentIndex == Birds.Count - 1)
                {
                    CurrentGameState = GameState.Lost;
                }
                else
                {
                    slingshot.slingshotstate = SlingshotState.Idle;
                    currentIndex++;
                    MoveToSlingshot();
                }
            });
    }

    private void Slingshot_BirdThrown(object sender, System.EventArgs e)
    {
        cameraFollow.BirdToFollow = Birds[currentIndex].transform;
        cameraFollow.IsFollow = true;
    }
    private bool AllPigDestroyed()
    {
        return Pigs.All(x => x == null);
    }
    bool BrickBirdPigStoppedMoving()
    {
        foreach( var item in Bricks.Union(Birds).Union(Pigs))
        {
            if(item != null && item.GetComponent<Rigidbody2D>().velocity.sqrMagnitude > 0.05f)
            {
                return false;
            }
        }
        return true;
    }
    void MoveToSlingshot()
    {  
        CurrentGameState = GameState.BirdMovingToSlingshot;
        Birds[currentIndex].transform.positionTo
            (Vector2.Distance(Birds[currentIndex].transform.position / 10,
            slingshot.BirdWaitPos.transform.position) / 10,
            slingshot.BirdWaitPos.transform.position).
            setOnCompleteHandler((x) =>
            {
                x.complete();
                x.destroy();
                CurrentGameState = GameState.Playing;
                slingshot.enabled = true;
                slingshot.BirdToThrown = Birds[currentIndex];
            });
    }
	IEnumerator showscoreboard(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		scoreboard.SetActive (true);
	}
	IEnumerator showleftstar(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		leftrong.SetActive (false);
		leftstar.SetActive (true);
	}
	IEnumerator showmidstar(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		midrong.SetActive (false);
		midstar.SetActive(true);
	}
	IEnumerator showrightstar(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		rightrong.SetActive (false);
		rightstar.SetActive(true);
	}
}
