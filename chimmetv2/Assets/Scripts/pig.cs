﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class pig : MonoBehaviour {


	public Text secondtext;
    public float Health = 150f;
    public Sprite SpriteShowWhenHurt;
    private float ChangeSpriteHealth;
    public GameObject other;
	public GameObject smoke;
    private Vector3 delta;
	public Text m_text;
	gamanager gm;

    void Start()
    {
        ChangeSpriteHealth = Health - 30f;
        delta.x = transform.position.x + 2;
        delta.y = transform.position.y + 1;
		GameObject game = GameObject.FindGameObjectWithTag ("manager");
		gm = game.GetComponent<gamanager> ();
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.GetComponent<Rigidbody2D>() == null)
            return;
        if (col.gameObject.tag == "Bird" || col.gameObject.tag == "egg")
        {
            GetComponent<AudioSource>().Play();
            Destroy(gameObject);
			Instantiate(smoke, transform.position, Quaternion.identity);
            Instantiate(other, delta, Quaternion.identity);

			gm.score += 3000;
			m_text.text = "SCORE: " + gm.score.ToString ();
			secondtext.text = gm.score.ToString();
        }
        else
        {
            float damage = col.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 10;
            Health -= damage;
            if (Health < ChangeSpriteHealth)
            {
				GetComponent<Animator>().enabled = false;
                GetComponent<SpriteRenderer>().sprite = SpriteShowWhenHurt;
            }
            if (Health <= 10)
            {
                Destroy(this.gameObject);
              	GetComponent<AudioSource>().Play();
                Instantiate(other, delta, Quaternion.identity);
				Instantiate(smoke, transform.position, Quaternion.identity);
				gm.score += 3000;
				m_text.text = "SCORE: " + gm.score.ToString ();
				secondtext.text = gm.score.ToString();
            }
        }
    }
}
